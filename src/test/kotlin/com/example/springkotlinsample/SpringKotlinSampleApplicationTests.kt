package com.example.springkotlinsample

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SpringKotlinSampleApplicationTests {

    @Test
    fun `should return hello world greeting`(@Autowired restTemplate: TestRestTemplate) {
        val entity = restTemplate.getForEntity<List<String>>("/")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.headers.contentType).isEqualTo(MediaType.APPLICATION_JSON_UTF8)
        assertThat(entity.body).isEqualTo(listOf("Hello World!"))
    }

    @Test
    fun `should work with webclient as well`(@Autowired webClient: WebTestClient) {
        webClient.get().exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$").isArray()
                .jsonPath("$[0]").isEqualTo("Hello World!")
    }
}
