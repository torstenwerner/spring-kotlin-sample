object Versions {
    const val projectVersion = "0.0.1-SNAPSHOT"
    const val gradleVersion = "5.4.1"
    /**
     * The version of the spring boot framework.
     */
    const val springBootVersion = "2.1.6.RELEASE"
    /**
     * The version of the spring dependency management gradle plugin.
     */
    const val dependencyManagementVersion = "1.0.7.RELEASE"
    const val kotlinVersion = "1.3.40"
}
