package com.example.springkotlinsample

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.BodyInserters.fromObject
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

@Configuration
class SimpleRoute {
    @Bean
    fun route() = router {
        GET("/") {
            ServerResponse.ok().body(fromObject(arrayOf("Hello World!")))
        }
    }
}
